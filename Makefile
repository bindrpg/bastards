filename=main
branch := $(shell git rev-parse --abbrev-ref HEAD)
output: ${filename}.pdf
${filename}.pdf: ${filename}.glg
${filename}.ind: ${filename}.idx $(wildcard *.tex)
	pdflatex ${filename}.tex
	makeindex ${filename}.idx
${filename}.glg: 
	pdflatex ${filename}.tex
	makeglossaries ${filename}
	pdflatex ${filename}.tex
tree:
	[ -e ../../config ] || ( echo "You don't have a local config repo" && exit 1 )
	git status
	git subtree -P config pull ../../config ${branch}
clean:
	rm -fr *.aux *.toc *.acn *.log *.ptc *.out *.idx *.ist *.glo *.glg *.gls *.acr *.alg *.ilg *.ind *.pdf
